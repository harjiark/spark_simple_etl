package bd.sp.q

/**
  * Created by harji on 30/05/17.
  */

import org.apache.spark.ml.clustering.KMeans
// $example off$
import org.apache.spark.sql.SparkSession
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.regression.LabeledPoint

object KMeansTest {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder.appName(s"${this.getClass.getSimpleName}").getOrCreate()

    val dataset = spark.read.format("libsvm").load(args(0))

//      Seq(55,57,58,70,71,73).toDS() // spark.sparkContext.textFile(args(0))//spark.read.load(args(0))//Vectors.dense(67,65,66,75,76,78);//
//    val parsedData = dataset.map(s => Vectors.dense(s.split(' ').map(_.toDouble))).cache()
    // Trains a k-means model.
    val kmeans = new KMeans().setK(2).setSeed(1L).setMaxIter(500)
    val model = kmeans.fit(dataset)


    // Evaluate clustering by computing Within Set Sum of Squared Errors.
    val WSSSE = model.computeCost(dataset)
    println(s"Within Set Sum of Squared Errors = $WSSSE")

    // Shows the result.
    println("Cluster Centers: ")
    model.clusterCenters.foreach(println)
    // $example off$

    spark.stop()
  }
}
