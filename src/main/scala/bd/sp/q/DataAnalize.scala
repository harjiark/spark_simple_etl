  package bd.sp.q

import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.mapreduce.{TableInputFormat, TableOutputFormat}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by harji on 18/05/17.
  */
object DataAnalize extends Serializable{

  final val tableName = "wifisensor"

  def main(args: Array[String]): Unit = {
    // set up HBase Table configuration
    val conf = HBaseConfiguration.create()
//    conf.set(TableOutputFormat.OUTPUT_TABLE, tableName)
    conf.set(TableInputFormat.INPUT_TABLE, tableName)
    conf.set("zookeeper.connection.timeout.ms", "20s")

    val sparkConf = new SparkConf().setAppName("Wifi-data-processing")
    val sc = new SparkContext(sparkConf)
    val hBaseRDD = sc.newAPIHadoopRDD(conf, classOf[TableInputFormat],
      classOf[org.apache.hadoop.hbase.io.ImmutableBytesWritable],
      classOf[org.apache.hadoop.hbase.client.Result])

    println(hBaseRDD.count())
    sc.stop()

  }
}
