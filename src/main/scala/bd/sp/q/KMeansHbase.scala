package bd.sp.q

import java.text.SimpleDateFormat
import java.util.Calendar

import unicredit.spark.hbase.HBaseConfig
import org.apache.spark.mllib.clustering.{KMeans, KMeansModel}
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.{SparkConf, SparkContext}
import unicredit.spark.hbase._


/**
  * Created by harji on 02/06/17.
  */
object KMeansHbase {

  final val tableName = "wifisensor"

  def main(args: Array[String]): Unit = {
    val name = "Example of read from HBase table"

    lazy val sparkConf = new SparkConf().setAppName(name)
    lazy val sc = new SparkContext(sparkConf)
    implicit val config = HBaseConfig() // Assumes hbase-site.xml is on classpath

    val families = Set("data", "analizeresult")
    val format = new SimpleDateFormat("ddMMyyyyhhmm")
    val currentTime = format.format(Calendar.getInstance().getTime())
    println(currentTime)

    val rdd = sc.hbase[String](tableName, families)

    val vectorAverage = rdd.map(v => Vectors.dense(v._2.get("analizeresult").get("mean").toDouble)).cache()

    val numClusters = 7
    val numIterations = 100
    val clusters = KMeans.train(vectorAverage, numClusters, numIterations)

    val WSSSE = clusters.computeCost(vectorAverage)
    println("Within Set Sum of Squared Errors = " + WSSSE)

    // Save and load model
    clusters.clusterCenters.foreach(println)
    val prediction = clusters.predict(vectorAverage)
    // rdd.zip(prediction)
    rdd.zip(prediction).map({ case (k, v) =>
      val content = Map("label" -> v)
        k -> content
    })

//      toHBase(tableName, "analizeresult")


  }

  def average[T](ts: Iterable[T])(implicit num: Numeric[T]) = {
    num.toDouble(ts.sum) / ts.size
  }

  def f[T](v: T) = v


}
