package bd.sp.q

import java.text.SimpleDateFormat
import java.util.Calendar


import org.apache.spark.{SparkConf, SparkContext}
import unicredit.spark.hbase._

/**
  * Created by harji on 18/05/17.
  */
object Analize {
  final val tableName = "wifisensor"

  def main(args: Array[String]): Unit = {
    val name = "Example of read from HBase table"

    lazy val sparkConf = new SparkConf().setAppName(name)
    lazy val sc = new SparkContext(sparkConf)
    implicit val config = HBaseConfig() // Assumes hbase-site.xml is on classpath

    val families = Set("data","analizeresult")
    val format = new SimpleDateFormat("ddMMyyyyhhmm")
    val currentTime = format.format(Calendar.getInstance().getTime())
    println(currentTime)

    val rdd = sc.hbase[String](tableName, families)
    rdd.map({ case (k, v) =>
      val v1 = v("data").filterKeys(key => (!key.equals("mac")))
      val vav = v1.map({ case (k, v) => v.toDouble })
      val aver = average(vav)
      val content = Map("mean"->aver.toString)
        k->content
     }
      ).toHBase(tableName, "analizeresult")

    println("type rdd: "+f(rdd))
    //    .saveAsTextFile("test-output"+currentTime)

  }

  def average[T](ts: Iterable[T])(implicit num: Numeric[T]) = {
    num.toDouble(ts.sum) / ts.size
  }

  def f[T](v: T) = v

}