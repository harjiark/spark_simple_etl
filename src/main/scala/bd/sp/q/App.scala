package org.sia.chapter03App

import java.lang
import java.lang.System

import net.thornydev.JsonHiveSchema
import org.apache.spark.SparkConf
//import org.apache.spark.sql.Row
//import bd.sp.q.Service
//import org.apache.spark.SparkContext
//import org.apache.spark.sql.SparkSession

//import org.apache.hadoop.hive.ql.exec.spark.session.SparkSession
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.Row

import org.apache.spark.sql.hive.HiveSessionState

case class Record(asin: String, helpful: String)
/**
  * @author ${user.name}import
  */
object App {

  def main(args : Array[String]) {
    //    System.setProperty("spark.serializer", "spark.KryoSerializer")// kryo is much faster
    //    System.setProperty("spark.kryoserializer.buffer.mb", "256") // I serialize bigger objects
    //    System.setProperty("spark.mesos.coarse", "true")// link provided
    //    System.setProperty("spark.akka.frameSize", "500") // workers should be able to send bigger messages
    //    System.setProperty("spark.akka.askTimeout", "30") //
    val warehouseLocation = "/hive/warehouse"
    //      "/user/hive/warehouse"
    ////      "spark-warehouse"
    //      .config("spark.sql.warehouse.dir", warehouseLocation)
    //    hive.metastore.warehouse.dir
    //    spark.sql.warehouse.dir
    //    .config("spark.driver.maxResultSize","2g")
    //    ("spark.sql.warehouse.dir", warehouseLocation)
    //    .set("spark.driver.maxResultSize","2g")
    var args_list      = args.toList
    var conf = new SparkConf()
      .set("spark.sql.warehouse.dir", warehouseLocation)
      //      .set("spark.driver.maxResultSize","2048m")
      .set("spark.kryoserializer.buffer.max","1024m")
      //      .set("spark.yarn.driver.memoryOverhead","1024m")
      //      .set("spark.yarn.executor.memoryOverhead","1024m")
      //      .set("spark.yarn.executor.memoryOverhead","1024m")
      .set("spark.driver.extraClassPath","/opt/cloudera/parcels/CDH/lib/hive-hcatalog/share/hcatalog/")
      .set("spark.executor.extraClassPath","/opt/cloudera/parcels/CDH/lib/hive-hcatalog/share/hcatalog/")
      .set("spark.executor.extraJavaOptions","-XX:+PrintGCDetails -XX:+PrintGCTimeStamps")
    //      .setJars(Array("/opt/cloudera/parcels/CDH/lib/hive-hcatalog/share/hcatalog/hive-hcatalog-core.jar"))

    //    /opt/cloudera/parcels/CDH/lib/hive-hcatalog/share/hcataloghive-hcatalog-core.jar,/var/lib/hadoop-hdfs/sparktest/auxjars/json-serde-1.3.8-SNAPSHOT-jar-with-dependencies.jar
    val spark = SparkSession
      .builder()
      .appName("Spark Hive Example")
      .config(conf)
      .enableHiveSupport()
      .getOrCreate()

    val table_view_name = args_list(0)
    val limit = args_list(1)

//    val df_addjar = spark.sql("ADD JAR /opt/cloudera/parcels/CDH/lib/hive-hcatalog/share/hcatalog/hive-hcatalog-core.jar")
//
//    var csv_df = spark.read.csv("/user/hive/csv/healt_ratings/ratings_Health_and_Personal_Care.csv")
//    csv_df.select("*").limit(10).show()

    spark.sql("use testing")
    var join_df = spark.sql("select * from tablejoinresults limit 10")
    join_df.write.mode("append").saveAsTable("test_ten_rows")
    //    "SELECT hp.asin, hp.helpful,hp.overall,hp.reviewerid,hp.reviewername,hp.reviewtext,hp.reviewtime,hp.summary,hp.unixreviewtime FROM testing.healtpersonalcare_reviews hp LEFT JOIN testing.health_ratings hr ON (hp.reviewerid = hr.reviewerid)
    //    var df2 = spark.sql("CREATE VIEW"+table_view_name+" as SELECT hp.reviewerid,hp.asin,hp.overall,hp.reviewername,hp.reviewtext,hp.summary FROM healtpersonalcare_reviews hp JOIN healts_ratings hr ON (hp.reviewerid = hr.reviewerid) limit "+limit)
//    var df_csv = spark.read.csv()
//    var df_use =spark.sql("use testing")

//    val df = spark.sql("SELECT hp.asin, hp.helpful,hp.overall,hp.reviewerid,hp.reviewername,hp.reviewtext,hp.reviewtime,hp.summary,hp.unixreviewtime FROM testing.healtpersonalcare_reviews hp LEFT JOIN testing.health_ratings hr ON (hp.reviewerid = hr.reviewerid) ")
//    df.write.mode("append").saveAsTable("tablejoinresults")
    

    System.exit(0)
  }

}
