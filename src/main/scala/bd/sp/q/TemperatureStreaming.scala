package bd.sp.q

import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.dstream.ReceiverInputDStream
import org.apache.spark.streaming.mqtt.{MQTTInputDStream, MQTTUtils}
import org.apache.spark.streaming.{Seconds, StreamingContext, Time}

/**
  * @author Shivansh
  * @mail shiv4nsh@gmail.com
  */
object TemperatureStreaming extends App {

  val sparkConf = new SparkConf().setAppName("RaspberryPi-Temperature").setMaster("local[*]")
  val ssc = new StreamingContext(sparkConf, Seconds(5))

//  val brokerUrl =sparkConf.get("spark.mqtt.url")
//  val TOPIC = sparkConf.get("spark.mqtt.topic")
//  val userName = sparkConf.get("spark.mqtt.username")
//  val passWord = sparkConf.get("spark.mqtt.password")
//  val client_Id = sparkConf.get("spark.mqtt.clientid")
val brokerUrl ="tcp://192.168.5.79:1883"
  val TOPIC = "thilinamb/test"
  val userName = "testiot"
  val passWord = "testiot"
  val client_Id = "thilinamb-publisher"

//  createStream(ssc : org.apache.spark.streaming.StreamingContext, brokerUrl : scala.Predef.String, topic : scala.Predef.String, storageLevel : org.apache.spark.storage.StorageLevel, clientId : scala.Option[scala.Predef.String], username : scala.Option[scala.Predef.String], password : scala.Option[scala.Predef.String], cleanSession : scala.Option[scala.Boolean], qos : scala.Option[scala.Int], connectionTimeout : scala.Option[scala.Int], keepAliveInterval : scala.Option[scala.Int], mqttVersion : scala.Option[scala.Int])
  val temperatureStream = MQTTUtils.createStream(ssc,
    brokerUrl,
    TOPIC,
    StorageLevel.MEMORY_AND_DISK_SER_2
  )

//  client_Id,
//  userName,
//  passWord,
//  true,
//  20,
//  20,
//  20,
//  134

//  def createStream(
//                    ssc: StreamingContext,
//                    brokerUrl: String,
//                    topic: String,
//                    storageLevel: StorageLevel,
//                    clientId: Option[String],
//                    username: Option[String],
//                    password: Option[String],
//                    cleanSession: Option[Boolean],
//                    qos: Option[Int],
//                    connectionTimeout: Option[Int],
//                    keepAliveInterval: Option[Int],
//                    mqttVersion: Option[Int]
//                  ): ReceiverInputDStream[String] = {
//    new MQTTInputDStream(ssc, brokerUrl, topic, storageLevel, clientId, username, password,
//      cleanSession, qos, connectionTimeout, keepAliveInterval, mqttVersion)
//  }


  //  ,
//  username = None,
//  password = None,
//  cleanSession = true,
//  qos = 20,
//  connectionTimeout = 20,
//  keepAliveInterval = 20,
//  mqttVersion = None

//  MQTTUtils.createStream()

//  temperatureStream.compute(Time(5)).fold()(x => x)
  temperatureStream.foreachRDD(s => s.foreach(d => println(s"Message from Pi:$d")))
  ssc.start()
  ssc.awaitTermination()
}
