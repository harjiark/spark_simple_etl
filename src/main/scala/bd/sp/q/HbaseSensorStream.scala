package bd.sp.q

/**
  * Created by harji on 28/04/17.
  */

//import java.time.Duration

import org.apache.hadoop.hbase.{HBaseConfiguration, HColumnDescriptor, HTableDescriptor}
import org.apache.hadoop.hbase.client.{HBaseAdmin, Put}
import org.apache.hadoop.hbase.io.ImmutableBytesWritable
import org.apache.hadoop.mapreduce.Job
import org.apache.hadoop.hbase.mapreduce.TableOutputFormat
import org.apache.hadoop.hbase.util.Bytes

import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.Seconds
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.mqtt.MQTTUtils


object HBaseSensorStream extends Serializable {
  final val tableName = "wifisensor"
  final val cfDataBytes = Bytes.toBytes("data")
  final val cfAnalizingResult = Bytes.toBytes("analizingresult")
  final val cfTimeBytes = Bytes.toBytes("time")
  final val cfMacBytes = Bytes.toBytes("mac")
  final val colSignalBytes = Bytes.toBytes("sig")

  //  // schema for sensor data
  case class Sensor(time: String, mac: String, signal: String)

  object Sensor extends Serializable {
    // function to parse line of sensor data into Sensor class
    def parseSensor(str: String): Sensor = {
      print(str)
      val p = str.split("\\|")
      print("length of p: " + p.size)
      //      print(p)
      if (p.size == 3) {
        Sensor(p(0), p(1), p(2))
      } else {
        Sensor("empty", "empty", "empty")
      }
    }

    //  Convert a row of sensor object data to an HBase put object
    def convertToPut(sensor: Sensor): (ImmutableBytesWritable, Put) = {
      val dateTime = sensor.time
      // create a composite row key: sensorid_date time
      val rowkey = sensor.mac.trim
      val put = new Put(Bytes.toBytes(rowkey))
      //
      //      put.addColumn(cfDataBytes, cfTimeBytes, Bytes.toBytes(sensor.time))d
      put.addColumn(cfDataBytes, cfMacBytes, Bytes.toBytes(sensor.mac))
      put.addColumn(cfDataBytes, Bytes.toBytes(sensor.time.replaceAll(" ", "")), Bytes.toBytes(sensor.signal))
      return (new ImmutableBytesWritable(Bytes.toBytes(rowkey)), put)
    }

  }

  def main(args: Array[String]): Unit = {
    // set up HBase Table configuration
    val conf = HBaseConfiguration.create()
    conf.set(TableOutputFormat.OUTPUT_TABLE, tableName)
    conf.set("zookeeper.connection.timeout.ms", "20s")

    //create hbase table
    val admin = new HBaseAdmin(conf)
    if (!admin.isTableAvailable(tableName)) {
      val tableDesc = new HTableDescriptor(tableName)
      tableDesc.addFamily(new HColumnDescriptor("data".getBytes()))
      admin.createTable(tableDesc)
    }

    val job = Job.getInstance(conf)
    val jobConf = job.getConfiguration

    jobConf.set(TableOutputFormat.OUTPUT_TABLE, tableName)
    job.setOutputFormatClass(classOf[TableOutputFormat[ImmutableBytesWritable]])

    val sparkConf = new SparkConf().setAppName("RaspberryPi-WifiDetector")
    //      .setMaster("local[*]")
    val ssc = new StreamingContext(sparkConf, Seconds(10))
//    JavaStreamingContext ssc = new JavaStreamingContext(sparkConf, Seconds(10))
    val brokerUrl = "tcp://192.168.5.79:1883"
    val TOPIC = "thilinamb/test"
    val userName = "testiot"
    val passWord = "testiot"
    val client_Id = "thilinamb-publisher"
    /**
      *  def createStream(
      ssc: StreamingContext,
      brokerUrl: String,
      topic: String,
      storageLevel: StorageLevel,
      clientId: Option[String],
      username: Option[String],
      password: Option[String],
      cleanSession: Option[Boolean],
      qos: Option[Int],
      connectionTimeout: Option[Int],
      keepAliveInterval: Option[Int],
      mqttVersion: Option[Int]
    ): ReceiverInputDStream[String] = {
    new MQTTInputDStream(ssc, brokerUrl, topic, storageLevel, clientId, username, password,
          cleanSession, qos, connectionTimeout, keepAliveInterval, mqttVersion)
  }
      */

    //  createStream(ssc : org.apache.spark.streaming.StreamingContext, brokerUrl : scala.Predef.String, topic : scala.Predef.String, storageLevel : org.apache.spark.storage.StorageLevel, clientId : scala.Option[scala.Predef.String], username : scala.Option[scala.Predef.String], password : scala.Option[scala.Predef.String], cleanSession : scala.Option[scala.Boolean], qos : scala.Option[scala.Int], connectionTimeout : scala.Option[scala.Int], keepAliveInterval : scala.Option[scala.Int], mqttVersion : scala.Option[scala.Int])
    val wifiStream = MQTTUtils.createStream(
      ssc,
      brokerUrl,
      TOPIC,
      StorageLevel.MEMORY_AND_DISK_SER_2
    ).map(Sensor.parseSensor)

    wifiStream.foreachRDD { rdd =>
      // filter sensor data for low psi
      val alertRDD = rdd.filter(sensor => sensor.mac != "empty")
      //      alertRDD.take(1).foreach(println)
      // convert sensor data to put object and write to HBase table column family data
      alertRDD.map(Sensor.convertToPut).
        saveAsNewAPIHadoopDataset(jobConf)
      //      rdd.map(Sensor.convertToPut).
      //        saveAsHadoopDataset(jobConfig)
      rdd.foreach(d => println(s"Message from Pi:$d"))

    }
//    // Start the computation
    ssc.start()
    // Wait for the computation to terminate
    ssc.awaitTermination()

  }

}
