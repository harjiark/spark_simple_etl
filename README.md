# IOT-Hbase Random notes #

**=====================
MAPR START-STOP RITUALS
==================**

**Shutdown rituals:**

1. service mapr-warden stop
2. service mapr-zookeeper stop
3. ps -fu mapr
 
**Startup rituals:**

1. service mapr-zookeeper start
2. service mapr-zookeeper qstatus
3. service mapr-warden start
4. tail -f /opt/mapr/logs/warden.log
5. tail -f /opt/mapr/logs/cldb.log
6. maprcli node cldbmaster

**===================================
install and run mosquitto application
===================================**

* install mosquitto in ubuntu
* sudo apt-get install mosquitto(the mosquitto will automatically run with default setting)

**mosquitto start-stop manually**

**stop**

* sudo ss -lptn 'sport = :1883'(this is default mosquitto listening port)
* get the pid and execute command: kill -9 pid_number

**start**

    nohup mosquitto >  mosquitto.log &

**start kismet client & publisher**

* clone spark project repository  on https://harjiark@bitbucket.org/harjiark/spark_simple_etl.git
* in directory data/ copy file filterkismetlisten.rb to your rasberrypi environment
* make sure the ruby mqtt library installed there
* run that file using command : ruby filterkismetlisten.rb

**===================================**
**how to build & run spark application**
**===================================**

**1. Build spark application**

* clone spark project repository  on https://harjiark@bitbucket.org/harjiark/spark_simple_etl.git
* on root project run mvn clean && mvn install-> target directory will be created
* on target directory goto target/libs and copying spark-streamingtart -mqtt_2.11-2.0.1.jar & org.eclipse.paho.client.mqttv3-1.1.1.jar to /opt/mapr/spark/spark-2.1.0/jars
* on target/ copy the original-chapter03App-0.0.1-SNAPSHOT.jar to one of mapr node 
* 

**2. Run Spark Application**

    spark-submit --class bd.sp.q.TemperatureStreaming --master local[*] --deploy-mode client original-chapter03App-0.0.1-SNAPSHOT.jar